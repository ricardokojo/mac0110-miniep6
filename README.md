# MAC0110-MiniEP6

Repositório disponibilizado para a atividade MiniEP6 da disciplina MAC0110 - Introdução à Computação do 1º semestre de 2020, lecionada pelo professor Alfredo Goldman.

Este repositório contém:
* `miniep6.pdf` - enunciado do MiniEP6;
* `miniep6.jl` - arquivo Julia para a parte 2.1 e 2.2 da atividade;
* `respostas.txt` - arquivo texto para a parte 2.2 da atividade.

Caso tenham alguma dúvida durante o MiniEP6, mandem um e-mail para _ricardokojo@usp.br_.
