# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

#= Solução brute-force
   A ideia inicial do MiniEP6 era que todos os ímpares fossem retornados em um array
   Fiz apenas uma breve adaptação à solução inicial
   Há diversas outras formas de se resolver
   Para uma solução mais matemática, basta devolver n^2 - n + 1 =#
function impares_consecutivos(n)
    guess = [1 + (i * 2) for i in 0:n-1] # gera n ímpares
    cube_of_n = n ^ 3
    
    while sum(guess) != cube_of_n
        for i in 1:n
            guess[i] += 2
        end
    end
    
    return guess[1]
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    guess = [1 + (j * 2) for j in 0:m-1]
    cube_of_m = m ^ 3

    while sum(guess) != cube_of_m
        for k in 1:m
           guess[k] += 2
        end
    end

    print_formatter(m, cube_of_m, guess)
end

function print_formatter(n, cube_of_n, array)
    print("$(n) $(cube_of_n)")
    for el in array
        print(" $(el)")
    end
    println("")
end

function mostra_n(n)
    for i in 1:n
        imprime_impares_consecutivos(i)
    end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
mostra_n(20)
